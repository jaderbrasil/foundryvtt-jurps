JURPS
=====

Sistema de RPG próprio para Foundry VTT.

Além da ficha, diversos itens padrão já foram adicionados
ao jogo. É possivel arrastar e soltar os itens na ficha,
bem como certas características pré-definidas.


Ficha de Personagem
===================

.. image:: imgs/ficha-example-page1.jpg

.. image:: imgs/ficha-example-page1.2.jpg

.. image:: imgs/ficha-example-page2.jpg
   
.. image:: imgs/ficha-example-page4.jpg
   
Ficha de Armas
==============
.. image:: imgs/ficha-arma-example.jpg
   

Ficha de Features
=================
.. image:: imgs/ficha-features-example.jpg