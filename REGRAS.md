# Alterações de Armadura/Escudo codinome "agora vai"

- Dano Contundente e Pesado +1 cat dano contra shield.
- Dano de Armas Pesadas de Duas Mãos causam +1 cat dano contra shield.
- Diferença de tamanho, dá categoria de dano contra shield.
- O shield gasta 1 bloqueio para cada RES de dano sofrido em um único dano.
- O escudo quebra se chega a 0 bloqueios, reparar escudo exige
1 PO por RES por Bloqueio para aço, 25PC por cada RES por Bloqueio para madeira comum.
O preço que um ferreiro cobra pelo serviço é o dobro disso, sendo este o preço do material.


## Ataques Combinados

É possível agora, combinar ataques para causar o dano em conjunto,
somando as categorias, mas causando-o de uma única vez para quebrar o
shield do tanker safado.

Para isto, é necessário:
- É necessário que todos os envolvidos, concentrem até o momento do ataque,
exigindo foco total e todos os PAs da rodada.
- Um participante pode agir como Líder e conceder bônus baseado em influência,
podendo optar por DESVIOS ou fazer um teste para obter um modificador baseado
nos desvios de sucesso ou fracasso, podendo atrapalhar.
- Um teste para sincronizar o ataque, é um teste de Acerto comum, que acontece
na iniciativa do último participante. Se algum deles for interrompido, a manobra falha.


## Gold inicial
- 10 * (INF + 1/traço ou perícia que fizer sentido) PO
- E MAIS NADA.