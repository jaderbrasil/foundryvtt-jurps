/**
 * Extend the basic Item with some very simple modifications.
 * @extends {Item}
 */
export class JurpsItem extends Item {
  isFeature = () => this.type === "feature";

  /**
   * Augment the basic Item data model with additional dynamic data.
   */
  prepareData() {
    // As with the actor class, items are documents that can have their data
    // preparation methods overridden (such as prepareBaseData()).
    super.prepareData();
    this._updateEquipNumbers();
    this._updateWeaponNumbers();
  }

  /**
   * Prepare a data object which is passed to any Roll formulas which are created related to this Item
   * @private
   */
  getRollData() {
    // If present, return the actor's roll data.
    if (!this.actor) return null;
    const rollData = this.actor.getRollData();
    rollData.item = foundry.utils.deepClone(this.system);

    return rollData;
  }

  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */
  async roll() {
    const token = this.actor.token;

    let cardData = {
      ...this,
      actorId: this.actor.id,
      _id: this.id,
      tokenId: token?.uuid || null,
      isWeapon: this.type == "weapon",
      chatCard: true,
    };

    let content = undefined;
    if (this.type == "feature") {
      cardData.showControls = false;
      content = await renderTemplate("systems/jurps/templates/cards/feature-card.hbs", cardData);
    } else {
      content = await renderTemplate("systems/jurps/templates/cards/item-card.hbs", cardData);
    }

    const chatData = {
      user: game.user.id,
      speaker: ChatMessage.getSpeaker({ actor: this.actor }),
      roll: new Roll("0").evaluate({ async: false }), // Gambiarra
      content: content,
    };

    await ChatMessage.create(chatData);
  }

  _updateEquipNumbers() {
    if (this.type !== "equip") return;

    for (var res in this.system.resources) {
      let resource = this.system.resources[res];
      if (resource.value > resource.max) {
        resource.value = resource.max;
      }
    }
  }

  /**
   * Weapon damage.
   * @param {JurpsItem} item   The originating click event
   * @private
   */
  _updateWeaponNumbers() {
    if (this.type !== "weapon") return;

    let data = this.system;

    let vigreq = 4;
    let dsize, dweight;

    switch (data.info.size) {
      case 'small':
        data.info.sizeNum = 0;
        dsize = 1;
        if (data.info.dweight === "heavy") {
          data.info.dweight = "normal";
        }
        break;
      case 'medium':
        data.info.sizeNum = 1;
        dsize = 2;
        break;
      case 'large':
        data.info.sizeNum = 2;
        dsize = 3;
        vigreq += 2;
        if (data.info.trange === "ranged")
          data.info.usability = "twoHanded"
        break;
      case 'huge':
        data.info.sizeNum = 3;
        dsize = 4;
        vigreq += 4;
        data.info.dweight = "heavy";
        data.info.usability = "twoHanded";
        break;
    }

    switch (data.info.dweight) {
      case 'light':
        data.info.dweightNum = 0;
        dweight = 0;
        if (data.info.dusability === "oneAndHalf") {
          data.info.dusability = "oneHanded";
        }
        break;
      case 'normal':
        data.info.dweightNum = 1;
        dweight = 1;
        vigreq += 2;
        break;
      case 'heavy':
        data.info.dweightNum = 2;
        dweight = 1;
        vigreq += 4;
        break;
    }

    let duse = 0;
    let hitUsability = 0;
    let isDualWield = false;
    switch (data.info.usability) {
      case 'oneHanded':
        data.info.usabilityNum = 0;
        if (data.info.sizeNum > 1) {
          duse = -data.info.dweightNum;
        }
        break;

      case 'oneAndHalf':
        data.info.usabilityNum = 1;
        hitUsability = -(data.info.dweightNum); /* penalty */
        break;

      case 'twoHanded':
        data.info.usabilityNum = 2;
        break;

      case 'dualWield':
        data.info.usabilityNum = 3;
        isDualWield = true;
        hitUsability = -data.info.dweightNum * 2;
        if (data.info.sizeNum > 1) {
          hitUsability = hitUsability - (data.info.sizeNum - 1) * 2;
        }

        break;
    }

    let drange = 0;
    if (data.info.trange === "ranged") {
      if (!data.info.reloadDrum) data.info.reloadDrum = 1;
      data.info.range = data.info.sizeNum + 1;

      if (data.info.reload < 2) drange = -1;

      if (data.info.reloadDrum > 1 && (4 * data.info.reloadDrum) > data.info.reload) {
        drange = -Math.floor((3 * data.info.reloadDrum) / data.info.reload);
      } else if (data.info.reloadDrum == 1 && data.info.reload > 3) {
        drange = Math.floor((data.info.reload - 1) / 3.0);
      }
    }

    const damageFeature = (!isNaN(data.bonus.damageFeature)) ? data.bonus.damageFeature : 0;
    const vigreqFeature = (!isNaN(data.bonus.vigreqFeature)) ? data.bonus.vigreqFeature : 0;

    data.info.vigreq = vigreq + vigreqFeature;
    data.damage = drange + dweight + dsize + duse;
    if (isDualWield) data.damage *= 2;
    data.damage += damageFeature
    if (data.damage <= 0) data.damage = 1;

    data.hit = hitUsability + data.bonus.hit;
  }

}
