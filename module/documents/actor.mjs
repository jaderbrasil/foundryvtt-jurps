import * as utils from "../helpers/utils.mjs";

/**
 * Extend the base Actor document by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class JurpsActor extends Actor {

  /** @override */
  prepareData() {
    // Prepare data for the actor. Calling the super version of this executes
    // the following, in order: data reset (to clear active effects),
    // prepareBaseData(), prepareEmbeddedDocuments() (including active effects),
    // prepareDerivedData().
    super.prepareData();
  }

  /** @override */
  prepareBaseData() {
    // Data modifications in this step occur before processing embedded
    // documents or derived data.
    // console.log("prepareBaseData()")
    // console.log(this)
  }

  /**
   * @override
   * Augment the basic actor data with additional dynamic data. Typically,
   * you'll want to handle most of your calculated/derived data in this step.
   * Data calculated in this step should generally not exist in template.json
   * (such as ability modifiers rather than ability scores) and should be
   * available both inside and outside of character sheets (such as if an actor
   * is queried and has a roll executed directly from it).
   */
  prepareDerivedData() {
    // const actorData = this.data;
    // const data = actorData.data;
    // const flags = actorData.flags.jurps || {};

    // Make separate methods for each Actor type (character, npc, etc.) to keep
    // things organized.
    this._prepareCharacterData();
  }

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData() {
    if (this.type !== 'character') return;
    // Make modifications to data here.

    this.system.stats.mob.ajusteb = this._getEffectBonus(this.items, "mob") + this._getEquipsAdjust(this.items);
    this.system.stats.mob.esquivab = this._getEffectBonus(this.items, "esq");
    this.system.stats.mob.iniciativab = this._getEffectBonus(this.items, "ini");
    this.system.attributes.vig.current = this.system.attributes.vig.value + this._getEffectBonus(this.items, "vig");
    this.system.attributes.hab.current = this.system.attributes.hab.value + this._getEffectBonus(this.items, "hab");
    this.system.attributes.int.current = this.system.attributes.int.value + this._getEffectBonus(this.items, "int");
    this.system.attributes.con.current = this.system.attributes.con.value + this._getEffectBonus(this.items, "con");
    this.system.attributes.inf.current = this.system.attributes.inf.value + this._getEffectBonus(this.items, "inf");
    this.system.bonus.dmg = this._getEffectBonus(this.items, "dmg");

    const vig = this.system.attributes.vig.current;
    const habpenalty = this._checkEquipVigReq(this.items, vig);
    if (habpenalty > 0) this.system.attributes.hab.current -= habpenalty;

    const nvBonus = this.system.stats.level.value + this._getEffectBonus(this.items, "hplvl");
    const vigs = utils.sigma3(vig - 10);
    this.system.hp = vig + utils.hpLvlBonus(nvBonus, vigs);
    this.system.vida.max = this.system.hp;
    this.system.vida.min = -vig;

    const tentosb = this._getEffectBonus(this.items, "tentos");
    this.system.tentos.max = 2 + tentosb;

    const armorRes = this._getArmorRes(this.items);
    this.system.stats.res = armorRes.res;
    this.system.stats.rd = armorRes.rd;

    // Add level for easier access, or fall back to 0.
    if (this.system.stats.level) {
      this.system.nv = this.system.stats.level.value ?? 0;
      this.system.nvPoints = 7 + Math.floor((this.system.nv - 1) / 2);
    }

    // Attr shortcuts
    this.system.attrCost = 0;
    for (let [key, attr] of Object.entries(this.system.attributes)) {
      attr.passive = attr.current - 10;
      attr.sigma = utils.sigma3(attr.passive);
      this.system[key] = attr.current;
      this.system[key + "p"] = attr.passive;
      this.system[key + "s"] = attr.sigma;

      // this.system.attrCost += attr.value + 5 * utils.sigma3(attr.value - 10);
      this.system.attrCost += this._attrCost(attr.value);

      attr.label = game.i18n.localize(CONFIG.JURPS.attr[key]);
    }

    this.system.mobaj = this.system.stats.mob.ajusteb;
    this.system.esqb = this.system.stats.mob.esquivab;
    this.system.inib = this.system.stats.mob.iniciativab;
    this.system.ini = (this.system.mobaj < 0 ? this.system.mobaj : 0) + this.system.inib + this.system.habs + this.system.con;
    this.system.esq = this.system.esqb + this.system.mobaj + this.system.hab;
    this.system.mob = this.system.hab + this.system.mobaj;

    this.system.stats.slots.tra = 3 + Math.floor((this.system.nv - 1) / 5) + Math.floor((this.system.nv + 3) / 5);
    this.system.stats.slots.per = 2 + Math.floor((this.system.nv + 1) / 5);
    this.system.stats.slots.voc = 1 + Math.floor((this.system.nv + 2) / 5);
    this.system.stats.slots.dom = Math.floor(this.system.nv / 5);
  }

  _attrCost(value) {
    const COSTS = [-28, -20, -16, -12, -8, -6, -4, -2, -1, 0, 1, 2, 4, 6, 8, 12, 16, 20, 28, 36]
    if (value > 20) {
      return 100;
    }

    return COSTS[value - 1];
  }


  /**
   * @param JurpsItem[] items
   * @returns ({rd: number, res: number})
   * @private
   */
  _getArmorRes(items) {
    let armors = items.filter((item) => item.type === 'equip'
      && item.system.etype === 'armor'
      && item.system.equipped);

    if (armors.length === 0)
      return { 'res': 3, 'rd': 0 };

    const activeArmor = armors[0];
    return {
      'armorId': activeArmor._id,
      'res': activeArmor.system.info.res,
      'rd': activeArmor.system.resources.rd.value,
    };
  }

  /**
   * @param String effect
   * @returns total bonus of features
   * @private
   */
  _getEffectBonus(items, effect) {
    return items.reduce((a, b) => {
      if (b.type === 'equip' && !b.system.equipped) {
        return a;
      }

      //const bonus = getProperty(b.data, `data.bonus.${effect}`);
      const bonus = getProperty(b, `system.bonus.${effect}`);
      return a + (bonus ? bonus : 0);
    }, 0);
  }

  /**
   * @returns total bonus of equip/feat attr
   * @private
   */
  _checkEquipVigReq(items, vig) {
    return items.reduce((a, b) => {
      if (b.type !== "equip" || !b.system.equipped) return a;

      const vigreq = getProperty(b, `system.info.vigreq`);
      if (!vigreq || vigreq <= vig) return a;

      return a + (vigreq - vig);
    }, 0);
  }

  /**
   *
   * @returns mob adjust of equips
   * @private
   */
  _getEquipsAdjust(items) {
    // return items.reduce((a, b) => {
    //   if (b.data.type !== "item") return a;
    //   const armor = getProperty(b.data, `data.features.armor`);
    //   return a + (armor?.adj ? armor.adj : 0);
    // }, 0);
    return items.reduce((a, b) => {
      if (b.type !== "item") return a;
      const armor = getProperty(b, `system.features.armor`);
      return a + (armor?.adj ? armor.adj : 0);
    }, 0);
  }


  /**
   * Override getRollData() that's supplied to rolls.
   */
  getRollData() {
    const data = super.getRollData();

    // Prepare character roll data.
    this._getCharacterRollData(data);

    return data;
  }

  /**
   * Prepare character roll data.
   */
  _getCharacterRollData(data) {
    if (this.type !== 'character') return;
  }
}
