import * as Dice from "../dice.mjs";

export class JurpsActorSheet extends ActorSheet {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["jurps", "sheet", "actor"],
            width: 700,
            height: 927,
            top: 0,
            tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "info" }]
        });
    }

    /** @override */
    get template() {
        return `systems/jurps/templates/actor/actor-${this.actor.type}-sheet.hbs`;
    }

    /**
     * A helper method to establish the displayed preparation state for an equip.
     * @param {Item} item  Item being prepared for display. *Will be mutated.*
     * @private
     */
    _prepareEquipItems(item, resourceLabels) {
        /* ToggleState */
        const isActive = getProperty(item.system, "equipped");
        item.toggleClass = isActive ? "active" : "";
        item.toggleTitle = game.i18n.localize(isActive ? "JURPS.Item.Equipped" : "JURPS.Item.Unequipped");

        /* Resource Labels */
        for (var resource in item.system.resources) {
            item.system.resources[resource].label = game.i18n.localize(resourceLabels[resource]);
        }
    }

    /** @override */
    getData() {
        // Retrieve the data structure from the base sheet. You can inspect or log
        // the context variable to see the structure, but some key properties for
        // sheets are the actor object, the data object, whether or not it's
        // editable, the items array, and the effects array.
        const context = super.getData();

        // Use a safe clone of the actor data for further operations.
        const actorData = context.actor;

        // Add the actor's data to context.data for easier access, as well as flags.
        context.system = actorData.system;
        context.flags = actorData.flags;
        context.config = CONFIG.JURPS;

        context.weapons = [];
        context.equips = [];
        context.otherItems = [];
        context.traits = [];
        context.skills = [];
        context.vocations = [];
        context.gifts = [];
        context.annotations = [];

        context.items.forEach((item) => {
            switch (item.type) {
                case "weapon":
                    item.system.actorId = actorData._id;
                    item.system.tokenId = context.actor.token?.uuid || null;
                    context.weapons.push(item);
                    break;
                case "equip":
                    this._prepareEquipItems(item, CONFIG.JURPS.equipsResources);
                    context.equips.push(item);
                    break;
                case "item":
                    context.otherItems.push(item);
                    break;
                case "feature":
                    let ftype = `${item.system.ftype.toLowerCase()}s`;
                    if (context[ftype] === undefined) {
                        console.error(`actor-sheet.mjs:77 => ${ftype} is not a valid feature type.`);
                        break;
                    }
                    context[ftype].push(item);
                    break;
                case "annotation":
                    context.annotations.push(item);
                    break;
            }
        });

        // Add roll data for TinyMCE editors.
        context.rollData = context.actor.getRollData();

        return context;
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        // Render the item sheet for viewing/editing prior to the editable check.
        html.find('.item-edit').click(ev => {
            const itemId = $(ev.currentTarget).parents(".item").data("itemId");
            const item = this.actor.items.get(itemId);
            item.sheet.render(true);
        });

        // -------------------------------------------------------------
        // Everything below here is only needed if the sheet is editable
        if (!this.isEditable || !this.actor.isOwner) return;

        html.find(".item-create").click(this._onItemCreate.bind(this));
        html.find(".inline-edit").change(this._onItemEdit.bind(this));
        html.find(".item-roll").click(this._onItemRoll.bind(this));
        html.find(".task-check").click(this._onTaskCheck.bind(this));
        html.find(".reveal-rollable").on("mouseover mouseout", this._onToggleRollable.bind(this));

        // Item State Toggling
        html.find(".item-toggle").click(this._onToggleItem.bind(this));

        // Delete Inventory Item
        html.find('.item-delete').click(event => {
            const itemId = $(event.currentTarget).parents(".item").data("itemId");
            const item = this.actor.items.get(itemId);

            item.delete();
            this.render(false);
        });
    }

    async _onToggleRollable(event) {
        const rollables = event.currentTarget.getElementsByClassName("rollable");
        $.each(rollables, (index, value) => {
            $(value).toggleClass("hidden");
        });
    }

    /**
     * Handle toggling the state of an Owned Item within the Actor.
     * @param {Event} event        The triggering click event.
     * @returns {Promise<Item>}  Item with the updates applied.
     * @private
     */
    async _onToggleItem(event) {
        event.preventDefault();
        const itemId = event.currentTarget.closest(".item").dataset.itemId;
        const item = this.actor.items.get(itemId);

        const attr = "system.equipped";
        const isEquipped = getProperty(item, attr);

        console.log(this.actor.prototypeToken.actorLink);
        if (this.actor.prototypeToken.actorLink) {
            if (item.system.etype == "armor" && !isEquipped) {
                this.actor.items
                    .filter((item) => item.type === 'equip' &&
                        item.system.etype === 'armor' &&
                        item.system.equipped)
                    .forEach((armor) => armor.update({ "system.equipped": false }));
            }
        }

        return item.update({ [attr]: !isEquipped });
    }

    async _onTaskCheck(event) {
        Dice.TaskCheck({
            actionValue: event.currentTarget.dataset.actionValue,
            attrName: event.currentTarget.dataset.attrName,
            actorName: this.actor.name,
            askForOptions: (event.shiftKey != game.settings.get("jurps", "showTaskCheckOptions")),
        });
    }

    async _onItemEdit(event) {
        event.preventDefault();
        const header = event.currentTarget;
        const itemId = header.closest(".item").dataset.itemId;
        let item = this.actor.items.get(itemId);
        let field = header.name;

        return item.update({ [field]: header.value });
    }

    async _onItemRoll(event) {
        event.preventDefault();
        const header = event.currentTarget;
        const itemId = header.closest(".item").dataset.itemId;
        let item = this.actor.items.get(itemId);

        await item.roll();
    }

    /**
     * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
     * @param {Event} event   The originating click event
     * @private
     */
    async _onItemCreate(event) {
        event.preventDefault();
        const header = event.currentTarget;
        // Get the type of item to create.
        const type = header.dataset.type;
        // Grab any data associated with this control.
        const data = duplicate(header.dataset);
        // Initialize a default name.
        const name = header.dataset.name ? game.i18n.localize(header.dataset.name) : "";

        if (type === "feature") {
            data.ftype = header.dataset.ftype;
        }

        // Prepare the item object.
        const itemData = {
            name: name,
            type: type,
            data: data
        };
        // Remove the type from the dataset since it's in the itemData.type prop.
        delete itemData.data["type"];

        // Finally, create the item!
        return await Item.create(itemData, { parent: this.actor });
    }
}
