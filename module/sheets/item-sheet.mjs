export class JurpsItemSheet extends ItemSheet {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            width: 870,
            height: 871,
            classes: ["jurps", "sheet", "item"]
        });
    }

    /** @override */
    get template() {
        return `systems/jurps/templates/item/${this.item.type}-sheet.hbs`;
    }

    /** @override */
    getData() {
        const context = super.getData();

        // Use a safe clone of the actor data for further operations.
        const itemData = context.item;

        // Add the actor's data to context.data for easier access, as well as flags.
        context.system = itemData.system;
        context.flags = itemData.flags;
        context.config = CONFIG.JURPS;

        if (itemData.type === "feature")
            context.system.featureBonuses = this._getBonusWithLabel(itemData);

        return context;
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);
        // -------------------------------------------------------------
        // Everything below here is only needed if the sheet is editable
        if (!this.isEditable) return;

        html.find(".item-create").click(this._onItemCreate.bind(this));
    }

    /**
     * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
     * @param {Event} event   The originating click event
     * @private
     */
    async _onItemCreate(event) {
        event.preventDefault();
        const header = event.currentTarget;
        // Get the type of item to create.
        const type = header.dataset.type;
        // Grab any data associated with this control.
        const data = duplicate(header.dataset);
        // Initialize a default name.
        const name = header.dataset.name ? game.i18n.localize(header.dataset.name) : "";

        // Prepare the item object.
        const itemData = {
            name: name,
            type: type,
            data: data
        };

        // Remove the type from the dataset since it's in the itemData.type prop.
        delete itemData.data["type"];

        // Finally, create the item!
        return await ItemFeature.create(itemData, { parent: this.item });
    }

    /**
     * @private
     */
    _getBonusWithLabel(itemData) {
        if (itemData.type !== "feature" || !itemData.system) return [];
        const bonuses = itemData.system.bonus;

        const keys = Object.keys(bonuses);
        return keys.map((name) => ({
            label: CONFIG.JURPS.featureBonuses[name],
            value: (bonuses[name] > 0 ? `+${bonuses[name]}` : bonuses[name])
        }));
    }
}
