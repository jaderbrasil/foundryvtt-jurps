/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */
 export const preloadHandlebarsTemplates = async function() {
  return loadTemplates([
    "systems/jurps/templates/actor/parts/actor-stats.hbs",
    "systems/jurps/templates/actor/parts/actor-feature.hbs",
    "systems/jurps/templates/actor/parts/actor-traits.hbs",
    "systems/jurps/templates/actor/parts/actor-weapons.hbs",
    "systems/jurps/templates/actor/parts/actor-items.hbs",
    "systems/jurps/templates/actor/parts/actor-annotations.hbs",
    "systems/jurps/templates/actor/parts/actor-equips.hbs",
    "systems/jurps/templates/cards/item-card.hbs",
    "systems/jurps/templates/cards/equip-card.hbs",
    "systems/jurps/templates/cards/feature-card.hbs",
    "systems/jurps/templates/cards/annotation-card.hbs",
    "systems/jurps/templates/cards/roll-card.hbs",
    "systems/jurps/templates/cards/task-check-dialog.hbs",
    "systems/jurps/templates/item/feature-editor.hbs",
  ]);
};
