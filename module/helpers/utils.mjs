export function sigma3(passive) {
    return Math.sign(passive) * Math.floor(Math.abs(passive / 3))
}

export function hpLvlBonus(nv, vigs) {
    const fac = ( 1 + Math.max(0, vigs) ) / (1 + Math.max(0, -1*vigs));
    return Math.floor(nv * fac);
}
