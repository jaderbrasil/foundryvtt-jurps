export const JURPS = {};

/**
JURPS.Item = {
  "Quantity"  : "JURPS.Item.Quantity",
}

JURPS.Sheet = {
  "NewWeapon"   : "JURPS.Sheet.NewWeapon",
  "NewItem"     : "JURPS.Sheet.NewItem",
  "NewTrait"    : "JURPS.Sheet.NewTrait",
  "NewSkill"    : "JURPS.Sheet.NewSkill",
  "NewVocation" : "JURPS.Sheet.NewVocation",
  "NewGift"     : "JURPS.Sheet.NewGift",
  "AddWeapon"   : "JURPS.Sheet.AddWeapon",
  "AddItem"     : "JURPS.Sheet.AddItem",
  "AddTrait"    : "JURPS.Sheet.AddTrait",
  "AddSkill"    : "JURPS.Sheet.AddSkill",
  "AddVocation" : "JURPS.Sheet.AddVocation",
  "AddGift"     : "JURPS.Sheet.AddGift"
};
*/
/* Config */
JURPS.Stats = {
  Tentos: "JURPS.Stats.Tentos",
  Vida: "JURPS.Stats.Vida",
  Level: "JURPS.Stats.Level",
  Dodge: "JURPS.Stats.Dodge",
  Init: "JURPS.Stats.Init",
  Atual: "JURPS.Stats.Atual", /* used in dice.mjs (attack) */
  Max: "JURPS.Stats.Max",
  Acerto: "JURPS.Stats.Acerto",
  Mobility: "JURPS.Stats.Mobility",
};

JURPS.DamageTableMultiD = ["1", "d4", "d6", "d8", "d10", "d12", "3d6"];

JURPS.attr = {
  vig: "JURPS.Attr.vig",
  hab: "JURPS.Attr.hab",
  int: "JURPS.Attr.int",
  con: "JURPS.Attr.con",
  inf: "JURPS.Attr.inf"
};

JURPS.Features = {
  "Trait": "JURPS.Features.Trait",
  "Skill": "JURPS.Features.Skill",
  "Vocation": "JURPS.Features.Vocation",
  "Gift": "JURPS.Features.Gift"
};

JURPS.damageAttr = {
  none: "JURPS.None",
  vig: "JURPS.Attr.vig",
  hab: "JURPS.Attr.hab",
  int: "JURPS.Attr.int",
  con: "JURPS.Attr.con",
  inf: "JURPS.Attr.inf"
};

JURPS.featureBonuses = {
  ini: "JURPS.Stats.Init",
  esq: "JURPS.Stats.Dodge",
  mob: "JURPS.Stats.Mobility",
  hplvl: "JURPS.Bonus.HPLevel",
  title: "JURPS.Bonus.Title",
};

JURPS.equipsResources = {
  rd: "JURPS.Item.armor.rd",
  blocks: "JURPS.Item.shield.blocks",
};

JURPS.weaponsLabels = {
  size: {
    small: "JURPS.Item.weapons.small",
    medium: "JURPS.Item.weapons.medium",
    large: "JURPS.Item.weapons.large",
    huge: "JURPS.Item.weapons.huge",
  },

  dweight: {
    light: "JURPS.Item.weapons.light",
    normal: "JURPS.Item.normal",
    heavy: "JURPS.Item.weapons.heavy",
  },

  usability: {
    oneHanded: "JURPS.Item.weapons.oneHanded",
    twoHanded: "JURPS.Item.weapons.twoHanded",
    oneAndHalf: "JURPS.Item.weapons.oneAndHalf",
    dualWield: "JURPS.Item.weapons.dualWield",
  },

  difficulty: {
    simple: "JURPS.Item.weapons.simple",
    normal: "JURPS.Item.normal",
    complex: "JURPS.Item.weapons.complex",
  },

  trange: {
    throw: "JURPS.Item.weapons.throw",
    melee: "JURPS.Item.weapons.melee",
    long: "JURPS.Item.weapons.long",
    ranged: "JURPS.Item.weapons.ranged",
  }
};

JURPS.equipTypes = {
  armor: "JURPS.Item.armor.label",
  shield: "JURPS.Item.shield.label",
  //bag      : "JURPS.Item.bag.label",
  other: "JURPS.Item.equip.other",
};

JURPS.resultMsg = {
  success: "sucesso",
  failure: "fracasso",
};

JURPS.condSeverityLevel = [
  "JURPS.Messages.condSeverity.slight",
  "JURPS.Messages.condSeverity.medium",
  "JURPS.Messages.condSeverity.severe",
  "JURPS.Messages.condSeverity.extreme",
];