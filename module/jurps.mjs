// Import document classes.
import { JurpsActor } from "./documents/actor.mjs";
import { JurpsItem } from "./documents/item.mjs";
// Import sheet classes.
import { JurpsActorSheet } from "./sheets/actor-sheet.mjs";
import { JurpsItemSheet } from "./sheets/item-sheet.mjs";
// Import helper/utility classes and constants.
import { preloadHandlebarsTemplates } from "./helpers/templates.mjs";
import { JURPS } from "./helpers/config.mjs";

import * as dice from './dice.mjs';
import * as macros from "./macros.mjs";
import * as chat from "./chat.mjs";
import * as utils from "./helpers/utils.mjs";

Hooks.once("init", async function () {
  console.log("jurps | Initializing JURPS");

  // Add utility classes to the global game object so that they're more easily
  // accessible in global contexts.
  game.jurps = {
    JurpsActor,
    JurpsItem,
    macros: macros
  };

  CONFIG.JURPS = JURPS;

  CONFIG.Combat.initiative = {
    formula: "10 + @ini - 3d6 + @attributes.hab.current/100",
    decimals: 2
  };

  // Define custom Document classes
  CONFIG.Actor.documentClass = JurpsActor;
  CONFIG.Item.documentClass = JurpsItem;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("jurps", JurpsActorSheet, { makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("jurps", JurpsItemSheet, { makeDefault: true });

  registerSystemSettings();

  return preloadHandlebarsTemplates();
});

function registerSystemSettings() {
  game.settings.register("jurps", "systemMigrationVersion", {
    config: false,
    scope: "world",
    type: String,
    default: "",
  });

  game.settings.register("jurps", "showTaskCheckOptions", {
    config: true,
    scope: "client",
    name: "SETTINGS.showTaskCheckOptions.name",
    hint: "SETTINGS.showTaskCheckOptions.label",
    type: Boolean,
    default: false,
  });

  game.settings.register("jurps", "damageProgression", {
    config: true,
    scope: "world",
    name: "SETTINGS.damageProgression.name",
    hint: "SETTINGS.damageProgression.hint",
    type: String,
    choices: {
      MultiD: "SETTINGS.damageProgression.types.MultiD",
      D3: "SETTINGS.damageProgression.types.D3"
    },
    default: "MultiD"
  });

  game.settings.register("jurps", "damageForCondition", {
    config: true,
    scope: "world",
    name: "SETTINGS.damageForCondition.name",
    hint: "SETTINGS.damageForCondition.hint",
    type: Number,
    default: 4
  });

  game.settings.register("jurps", "damageCondMethod", {
    config: true,
    scope: "world",
    name: "SETTINGS.damageCondMethod.name",
    hint: "SETTINGS.damageCondMethod.hint",
    type: String,
    choices: {
      Log2: "SETTINGS.damageCondMethod.types.Log2",
      Multi: "SETTINGS.damageCondMethod.types.Multi"
    },
    default: "Log2"
  });
}

/* -------------------------------------------- */
/*  Handlebars Helpers                          */
/* -------------------------------------------- */

// If you need to add Handlebars helpers, here are a few useful examples:
Handlebars.registerHelper('concat', function () {
  var outStr = '';
  for (var arg in arguments) {
    if (typeof arguments[arg] != 'object') {
      outStr += arguments[arg];
    }
  }
  return outStr;
});

Handlebars.registerHelper({
  eq: (v1, v2) => v1 === v2,
  ne: (v1, v2) => v1 !== v2,
  lt: (v1, v2) => v1 < v2,
  gt: (v1, v2) => v1 > v2,
  lte: (v1, v2) => v1 <= v2,
  gte: (v1, v2) => v1 >= v2,
  and() {
    return Array.prototype.every.call(arguments, Boolean);
  },
  or() {
    return Array.prototype.slice.call(arguments, 0, -1).some(Boolean);
  },
  not: (v1) => !v1,
});

Handlebars.registerHelper('toLowerCase', function (str) {
  if (str == null) {
    return ''
  }
  return str.toLowerCase();
});

Handlebars.registerHelper("getDice", (damage) => dice.getDiceCategory(damage));
Handlebars.registerHelper("getWeaponDamage", (weapon) => {
  if (!weapon.system) {
    return 0
  }

  let damage = parseInt(weapon.system.damage);

  if (['vig', 'hab', 'int', 'inf', 'con'].includes(weapon.system.bonus.damageAttr)) {
    let attrName = weapon.system.bonus.damageAttr;

    if (weapon.system.tokenId) {
      const tokenIdSplit = weapon.system.tokenId.split(".");
      const sceneId = tokenIdSplit[1];
      const tokenId = tokenIdSplit[3];
      const token = game.scenes.get(sceneId)?.tokens.get(tokenId) || null;
      // let attributes = token.data.actorData.data?.attributes;
      let attributes = token._actor.system.attributes;

      /* bug fix temporário */
      let attr;
      if (attributes) {
        attr = attributes[attrName]?.current || null;
      } else {
        attributes = game.actors.get(weapon.system.actorId)?.data?.data?.attributes;
        attr = attributes[attrName]?.current || null;
      }

      damage += token._actor.system.bonus.dmg || 0;
      damage += (attr) ? (utils.sigma3(parseInt(attr) - 10) || 0) : 0;
    } else {
      const actorData = game.actors.get(weapon.system.actorId);
      damage += actorData?.system.bonus.dmg || 0;
      damage += (parseInt(actorData?.system[`${attrName}s`]) || 0);
    }
  }

  return dice.getDiceCategory(damage)
});

Handlebars.registerHelper('ifNotEquals', function (arg1, arg2, options) {
  return (arg1 != arg2) ? options.fn(this) : options.inverse(this);
});

/* -------------------------------------------- */
/*  Chat Hooks                                  */
/* -------------------------------------------- */
Hooks.on("renderChatLog", (_app, html, _data) => chat.addChatListeners(html));

/* -------------------------------------------- */
/*  Ready Hook                                  */
/* -------------------------------------------- */

Hooks.once("ready", async function () {
  // Wait to register hotbar drop hook on ready so that modules could register earlier if they want to
  Hooks.on("hotbarDrop", (_bar, data, slot) => macros.createMacro(data, slot));

  // Game Master Check
  if (!game.user.isGM) {
    return;
  }
});
