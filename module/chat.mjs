import * as Dice from "./dice.mjs";

export function addChatListeners(html) {
    html.on("click", "button.attack", onAttack);
    html.on("click", "button.damage", onDamage);
}

function _getTargetsData() {
    if (game.user.targets.size === 0)
        return [];

    return [...game.user.targets];
}

function _releaseTargets(targets) {
    targets.forEach((target) => {
        target.setTarget(false, { releaseOthers: false, groupSelection: true });
    });
}

async function onAttack(event) {
    const card = event.currentTarget.closest(".weapon-card");
    const askForOptions = (event.shiftKey != game.settings.get("jurps", "showTaskCheckOptions"));

    const actor = await _getChatCardActorData(card);
    if (!actor) return;

    let weapon = actor.items.get(card.dataset.itemId);

    const targets = _getTargetsData();
    let targetActor = undefined;
    if (targets.length > 0) {
        targetActor = targets[0].actor;
    }

    let res = Dice.AttackCheck(actor, weapon, targetActor, askForOptions);

    console.log(res);
    if (!await res) _releaseTargets(targets);
}

async function onDamage(event) {
    const card = event.currentTarget.closest(".weapon-card");
    const askForOptions = (event.shiftKey != game.settings.get("jurps", "showTaskCheckOptions"));

    const actor = await _getChatCardActorData(card);
    if (!actor) return;

    let weapon = actor.items.get(card.dataset.itemId);

    const targets = _getTargetsData();
    let targetActor = undefined;
    if (targets.length > 0) {
        targetActor = targets[0].actor;
    }

    Dice.DamageRoll(actor, weapon, targetActor, askForOptions);

    _releaseTargets(targets);
}

/**
 * Get the Actor which is the author of a chat card
 * @param {HTMLElement} card    The chat card being used
 * @returns {Actor|null}         The Actor entity or null
 * @private
 */
async function _getChatCardActorData(card) {
    // Case 1 - a synthetic actor from a Token
    if (card?.dataset?.tokenId || null) {
        const token = await fromUuid(card.dataset.tokenId);
        if (!token) return null;
        return token.actor; //?.data || null;
    }

    // Case 2 - use Actor ID directory
    const actorId = card.dataset.actorId;
    return game.actors.get(actorId) || null;
}
