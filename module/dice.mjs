import { JURPS } from "./helpers/config.mjs";
import * as utils from "./helpers/utils.mjs";

export async function TaskCheck({
    actionValue = 0,
    attrName = "",
    actorName = "",
    askForOptions = true,
} = {}) {
    const messageTemplate = "systems/jurps/templates/cards/roll-card.hbs";

    let bonusFromDialog = 0;
    if (askForOptions) {
        let checkOptions = await GetTaskCheckOptions(attrName);

        if (checkOptions.cancelled) {
            return;
        }

        bonusFromDialog = parseInt(checkOptions.bonusValue);
        if (isNaN(bonusFromDialog)) bonusFromDialog = 0;
        actionValue = parseInt(actionValue) + bonusFromDialog;
    }

    let rollFormula = "3d6"; // @actionValue -
    let rollData = {
        actionValue: actionValue,
    };
    let rollResult = new Roll(rollFormula, rollData).evaluate({ async: false });

    const margem = parseInt(actionValue) - parseInt(rollResult.total);

    const resultType = (margem >= 0) ? "success" : "failure";
    const resultTypeStr = `<b class="dice-${resultType}-msg">${game.i18n.localize(JURPS.resultMsg[resultType])}</b>`;
    const resultData = {
        actor: actorName,
        flavor: attrName,
        mod: bonusFromDialog,
        resultType: resultTypeStr
    };
    const resultMsg = game.i18n.format(`JURPS.Messages.successMsg${(askForOptions ? "WithBonus" : "")}`, resultData);

    let chatData = {
        ...rollResult,
        total: rollResult.total,
        tooltip: await rollResult.getTooltip(),
        formula: rollResult.formula,
        margem: margem,
        desvios: utils.sigma3(margem),
        actionValue: actionValue,
        attrName: attrName,
        messageType: resultType,
        resultMsg: resultMsg,
        actorName: actorName,
    };

    let renderedRoll = await renderTemplate(messageTemplate, chatData);

    let messageData = {
        speaker: ChatMessage.getSpeaker(),
        content: renderedRoll,
    };

    await rollResult.toMessage(messageData);
}

async function GetTaskCheckOptions(taskType) {
    const title = game.i18n.format("JURPS.Messages.taskCheckTitle", { taskType: taskType });
    return GetOptionsDialog(title);
}

async function GetDamageOptions() {
    const title = game.i18n.localize("JURPS.Messages.damageRollTitle");
    return GetOptionsDialog(title);
}

async function GetOptionsDialog(title) {
    const template = "systems/jurps/templates/cards/task-check-dialog.hbs";
    const html = await renderTemplate(template, {});

    return new Promise(resolve => {
        const data = {
            title: title,
            content: html,
            buttons: {
                normal: {
                    label: game.i18n.localize("JURPS.Messages.actions.roll"),
                    callback: html => resolve(_processTaskCheckOptions(html[0].querySelector("form")))
                },
                cancel: {
                    label: game.i18n.localize("JURPS.Messages.actions.cancel"),
                    callback: html => resolve({ cancelled: true })
                }
            },

            default: "normal",
            close: () => resolve({ cancelled: true })
        };

        new Dialog(data, null).render(true);
    });
}

function _processTaskCheckOptions(form) {
    return {
        bonusValue: form.bonus.value
    };
}

export async function AttackCheck(attacker, weapon, targetActor, askForOptions) {
    const messageTemplate = "systems/jurps/templates/cards/roll-card.hbs";
    let rollFormula = "3d6"; // @acerto -
    let acerto = parseInt(attacker.system.hab) + parseInt(weapon.system.hit);

    let bonusFromDialog = 0;
    if (askForOptions) {
        let checkOptions = await GetTaskCheckOptions(game.i18n.localize("JURPS.Messages.attackFlavor"));

        if (checkOptions.cancelled) {
            return;
        }

        bonusFromDialog = parseInt(checkOptions.bonusValue);
        if (isNaN(bonusFromDialog)) bonusFromDialog = 0;
        acerto += bonusFromDialog;
    }

    if (weapon.system.info.vigreq > attacker.system.vig) {
        const pen = weapon.system.info.vigreq - attacker.system.vig;
        acerto -= pen;
    }

    let targetData = undefined;
    if (targetActor !== undefined) {
        targetData = {
            'dodge': (targetActor.system.esq - 10),
            'name': targetActor.name,
        }
        acerto -= targetData.dodge;

        if (targetData.dodge >= 0) {
            targetData.dodge = `+${targetData.dodge}`;
        }
    }

    let rollResult = new Roll(rollFormula, {}).evaluate({ async: false }); //{} attacker.data
    const margem = parseInt(acerto) - parseInt(rollResult.total);

    const resultType = (margem >= 0) ? "success" : "failure";
    const resultTypeStr = `<b class="dice-${resultType}-msg">${game.i18n.localize(JURPS.resultMsg[resultType])}</b>`;
    const resultData = {
        actor: attacker.name,
        flavor: game.i18n.localize("JURPS.Messages.attackFlavor"),
        mod: bonusFromDialog,
        resultType: resultTypeStr
    };
    const resultMsg = game.i18n.format(`JURPS.Messages.successMsg${(askForOptions ? 'WithBonus' : '')}`, resultData)
        + (targetData ? game.i18n.format(`JURPS.Messages.targetDodge`, targetData) : '')
        + '.';



    let chatData = {
        ...rollResult,
        total: rollResult.total,
        tooltip: await rollResult.getTooltip(),
        flavor: `no ataque`,
        formula: rollResult.formula,
        margem: margem,
        desvios: utils.sigma3(margem),
        actionValue: acerto,
        attrName: game.i18n.localize(CONFIG.JURPS.Stats.Acerto),
        messageType: resultType,
        resultMsg: resultMsg,
        actorName: attacker.name,
    };

    let renderedRoll = await renderTemplate(messageTemplate, chatData);

    let messageData = {
        speaker: ChatMessage.getSpeaker(),
        content: renderedRoll,
    };

    await rollResult.toMessage(messageData);

    return margem >= 0;
}

export async function DamageRoll(attacker, weapon, targetActor, askForOptions) {
    let bonusFromDialog = 0;
    if (askForOptions) {
        let checkOptions = await GetDamageOptions()

        if (checkOptions.cancelled) {
            return;
        }

        bonusFromDialog = parseInt(checkOptions.bonusValue);
        if (isNaN(bonusFromDialog)) bonusFromDialog = 0;
    }

    let damageInfo = {
        'cond': 0,
        'severity': '',
    };

    if (targetActor !== undefined) {
        damageInfo.targetData = {
            'rd': targetActor.system.stats.rd,
            'name': targetActor.name,
        }
    }

    const rollFormula = getWeaponDamage(attacker,
        weapon,
        bonusFromDialog - (damageInfo.targetData ? damageInfo.targetData.rd : 0));
    const rollResult = new Roll(rollFormula).evaluate({ async: false });
    const renderedRoll = await rollResult.render();


    let damageForCond = game.settings.get('jurps', 'damageForCondition');
    let damageCondMethod = game.settings.get('jurps', 'damageCondMethod');

    let damageCondFunc = undefined;
    if (damageCondMethod === 'Log2') {
        damageCondFunc = (value) => Math.floor(Math.log2(value / damageForCond));
    } else {
        damageCondFunc = (value) => Math.floor(value / damageForCond) - 1;
    }

    if (damageForCond >= 0) {
        damageInfo.cond = damageCondFunc(rollResult.total) % CONFIG.JURPS.condSeverityLevel.length;

        if (damageInfo.cond >= 0) {
            damageInfo.dcat = getDiceCategory(damageInfo.cond + 1);
            damageInfo.severity = game.i18n.localize(
                CONFIG.JURPS.condSeverityLevel[damageInfo.cond]
            );
        }
    }

    if (damageInfo.targetData) {
        const newHp = targetActor.system.vida.value - rollResult.total;

        if (Number.isInteger(newHp) && targetActor.isOwner) {
            await targetActor.update({ ['system.vida.value']: newHp });
        }

    }

    const resultMsg = game.i18n.format('JURPS.Messages.damageMsg', { weaponName: weapon.name, rollFormula: rollFormula })
        + (damageInfo.targetData ? game.i18n.format('JURPS.Messages.targetRD', damageInfo.targetData) : '')
        + (damageInfo.cond >= 0 ? game.i18n.format('JURPS.Messages.targetCondDmg', damageInfo) + `[${damageInfo.cond + 1} | ${damageInfo.dcat}]` : '')
        + (damageInfo.cond >= 1 ? '. ' + game.i18n.localize('JURPS.Messages.actions.qualityCheck') : '')
        + '.';

    const messageData = {
        speaker: ChatMessage.getSpeaker(),
        content: renderedRoll,
        flavor: resultMsg,
    };

    await rollResult.toMessage(messageData);
}

export function getWeaponDamage(attacker, weapon, bonus) {
    let damage = parseInt(weapon.system.damage) + bonus;

    if (['vig', 'hab', 'int', 'inf', 'con'].includes(weapon.system.bonus.damageAttr)) {
        let attrs = attacker.system[`${weapon.system.bonus.damageAttr}s`];
        damage += parseInt(attrs);

        if (weapon.system.damage > 0 && damage <= 0) damage = 1;
    }

    let dmgbonus = attacker.system.bonus.dmg || 0;

    return getDiceCategory(damage + dmgbonus);
}

export function getDiceCategory(damage) {
    if (damage < 0) return 0;

    let damageProgression = game.settings.get("jurps", "damageProgression");
    if (damageProgression === 'MultiD') {
        if (damage < JURPS.DamageTableMultiD.length) return JURPS.DamageTableMultiD[damage];

        var amount = 4 + (damage - JURPS.DamageTableMultiD.length);
        return `${amount}d6`;
    } else if (damageProgression === 'D3') {
        if (damage == 0) return '1d3-1';

        return `${damage}d3`;
    }
};
