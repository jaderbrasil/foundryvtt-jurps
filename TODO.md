## Bugs
- [ ] Ao fechar aficha e clicar Atacar na arma rolada, da NaN no acerto. Talvez algum await que faltou?
- [○] Goblin Wood Maze Espada curta não rola -1/+1 de dano (dice.mjs:185 DamageRoll) (F5 resolveu????)
- [ ] Bug ao abrir ficha de token
- [ ] Ficha de Observador tem um "bug" na ui, onde os botões de "Excluir",
      "Editar", ficam "acesos" como se fossem funcionar, mas não funcionam.
- [ ] Machados estão errados (Machado de Batalha pelo menos)

## Mudanças
- [✓] O dano mínimo é d4, nunca deve cair disso.

## Features
- [ ] Adicionar uma sessão de comentário que só o GM pode ver?
- [ ] Aba com Editor para descritores
- [ ] Aba com Editor para usabilidade onde o personagem seleciona seus Traços, Perícias e Dons que dão bônus para o uso da arma e qual é este bônus
- [ ] sistema de mochilas, algibeiras
- [ ] onde armazenar recursos
- [ ] efeitos ativos ao equipar item? (tocha, lanterna)

## Combate melhor
- [ ] vincular disparo com munição em armas (usar o editor de traços para isso?)
- [ ] Uma única armadura equipada por vez
- [ ] Um único escudo equipado por vez
- [ ] Ao clicar no alvo, um diálogo pergunta se ele quer Bloquear ou Esquivar e
      exibe na tela. Ao esquivar, pede o modificador de acerto para o atacante,
      se ele confirma a rolagem, já pega a Esquiva. O mesmo para Escudos,
      pegando absorção e descontando bloqueios. Vida é removida e exibe no chat
      quando for o caso.
- [ ] Sistema de condição aleatória e aplica automaticamente.

## Conteúdo
- [ ] csv com itens
